// CRUD Operations in MongoDB (As discussed)
// To execute the code in Robo3T, press F5 or Control + Enter


// C - CREATE (Or INSERT)

	// INSERT ONE: To insert one document, we have used the method "insertOne"
	db.users.insertOne({
		firstName: "John",
		lastName: "Smith"
	});

	// INSERT MANY: To insert multiple documents, we can use insertMany()
	db.users.insertMany([
		{ firstName: "John", lastName: "Doe" },
		{ firstName: "Jane", lastName: "Doe" }
	]);


// R - READ

	// FIND: To get all the inserted users, we can use find()
	db.users.find();

	// FIND SPECIFIC: To find documents with a specified condition, an object should be passed to the find().
	db.users.find({ lastName: "Doe" })
	// - This will find all users with Doe on their lastName


// U- UPDATE

	// UPDATE ONE:
	db.users.updateOne(
		{
			_id: ObjectId("6347dce61be9c935ac8dcd5c")
		},
		{
			$set: {
				email: "johnsmith@gmail.com"
			}
		}
	);
	// - The first argument contains the identifier on which documents to update and the second argument contains the properties to be updated
	// - Since email did not exist before the update, updateOne() will simply add the email property to the selected document. 

	// UPDATE MANY: 
	db.users.updateMany(
		{
			lastName: "Doe"
		},
		{
			$set: {
				isAdmin: false
			}
		}
	);
	// - Now, all documents with last name of "Doe" has a property of isAdmin with a value of false. 

// D - DELETE

// DELETE MANY: To update multiple documents, we can use deleteMany()
db.users.deleteMany({ lastName: "Doe" });
// - Be careful when deleting data as you may delete data accidentally! 

// DELETE ONE: We can use deleteOne instead when we're sure we want to delete only one document.
db.users.deleteOne({ _id: ObjectId("62b54b31b55e350b5950bb76") });


// How we operate on MongoDB and Robo3T 
// 1. Right click on the new connections. Then on the drop down choices, click "Create Database"
// 2. Decide on the name of the database
// 3. Right click on the new made database. Click on "Open Shell"
// 4. Type the code on the field where you are allowed to type the code
// 5. To execute the code, press F5 or Control + Enter

// What we did on MongoDB
// 1. We registered through gmail
// 2. We have chosen Amazon and Singapore respectively. After that, we have decided on the name of the cluster
// 3. We have added an IP on the Network Access. We have clicked "Allow access from anywhere"
// 4. We have set our username and password on the Quickstart