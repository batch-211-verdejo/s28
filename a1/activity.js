/*
1. Create an "activity.js" and create "room" documents to simulate a hotel's database.
	Device > Terminal/activity.js
*/

		// Inserting a single document
		db.rooms.insertOne({
		    name: "single",
		    accomodates: 2,
		    price: 1000,
		    description: "A simple room with all the basic necessities",
		    rooms_available: 10,
		    isAvailable: false
		});

		// Inserting multiple documents
		db.rooms.insertMany([
		    {
		        name: "double",
		        accomodates: 3,
		        price: 2000,
		        description: "A room fit for a small family going on a vacation",
		        rooms_available: 5,
		        isAvailable: false
		    },
		    {
		        name: "queen",
		        accomodates: 4,
		        price: 4000,
		        description: "A room with a queen sized bed perfect for a simple getaway",
		        rooms_available: 15,
		        isAvailable: false
		    }
		]);

/*
2. Use the "find" method to search for a specific room.
	Device > Terminal/activity.js
*/

		// Find a document
		db.rooms.find({ name: "double" }).pretty();

/*
3. Use the "updateOne" method to update a specific room.
	Device > Terminal/activity.js
*/

		// Updating a document
		db.rooms.updateOne(
		    { name: "queen" },
		    {
		        $set : {
		            rooms_available: 0
		        }
		    }
		);

		db.rooms.find({ name: "queen" }).pretty();

/*
4. Use the "deleteMany" method to delete rooms.
	Device > Terminal/activity.js
*/

		// Deleting multiple documents
		db.rooms.deleteMany({
		    rooms_available: 0
		})

		db.rooms.find().pretty();
