db.rooms.insertOne(
    {
        name:"single",
        accomodates: "2",
        price: "1000",
        description: "A simple room with all the basic necessities",
        rooms_available: "5",
        isAvailable: "false"
    });


db.rooms.insertMany([
    {
        name:"double",
        accomodates: "3",
        price: "2000",
        description: "A room fit for a small family going on a vacation",
        rooms_available: "5",
        isAvailable: "false"
    },
    {
        name:"queen",
        accomodates: "4",
        price: "4000",
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: "15",
        isAvailable: "false"
    }]);

// Get the list of collections with the keyword "rooms".
db.getCollection("rooms").find({});

// Find method
db.rooms.find({name:"double"})

// Update 1 object
db.rooms.updateOne(
    {
        _id: ObjectId("6345e716cb34aa2287267262")
    },
    {
        $set: {
            rooms_available: "0"
        }
    }
);

// deleteMany method
db.rooms.deleteMany({rooms_available: "0"});

